from importlib import machinery, util
import os
import yaml


class Config:
    def __init__(self):
        with open(os.path.dirname(os.path.realpath(__file__)) + "/config.yaml", "r") as configfile:
            cfg = yaml.full_load(configfile)
        self.__conf = cfg

    
    @property
    def config(self):
        configuration = self.__conf
        newconfig_spec = machinery.ModuleSpec("config", None)
        newconfig = util.module_from_spec(newconfig_spec)
        newconfig.log_filepath = configuration["log_filepath"]
        newconfig.jira_api = configuration["jira_api"]
        newconfig.allowed_extensions = configuration["allowed_extensions"]
        newconfig.jql_query = configuration["jql_query"]
        newconfig.excel_admarker_field = configuration["excel_admarker_field"]
        newconfig.xml_admarker_xpaths = configuration["xml_admarker_xpaths"]
        newconfig.jira_admarker_field = configuration["jira_admarker_field"]
        newconfig.pause_between_calls = configuration["pause_between_calls"]
        newconfig.jira_user = configuration["jira_user"]
        newconfig.jira_password = configuration["jira_password"]
        return newconfig


