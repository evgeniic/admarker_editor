import logging
from admarker_config import Config

config = Config().config

def get_logger(logfile=config.log_filepath):
    """
	Provide logger object bind to a file.
	"""
    logging.basicConfig(
        level=logging.INFO,
        format="[ADMARKER][%(asctime)s][module=%(module)s][thread=%(threadName)s] %(levelname)s: %(message)s",
    )
    # Different level for POST calls
    logging.getLogger("requests").setLevel(logging.WARNING)
    # For every module write to the same logfile
    logger = logging.getLogger(__name__)
    handler = logging.FileHandler(logfile)
    formatter = logging.Formatter(
        "[ADMARKER][%(asctime)s][module=%(module)s][thread=%(threadName)s] %(levelname)s: %(message)s"
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger


def debug_mode_on():
    logging.root.setLevel(logging.DEBUG)
    logging.getLogger("requests").setLevel(logging.WARNING)


def debug_mode_off():
    logging.root.setLevel(logging.INFO)
    logging.getLogger("requests").setLevel(logging.WARNING)


# Add in every function this
# logging.debug('Function called: %s\n
# 					Values passed: (%s)' % (f.__name__, parameters))
