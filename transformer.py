import os
from openpyxl import load_workbook
from lxml import etree
import re

import Log
from admarker_config import Config
logger = Log.get_logger()
config = Config().config


class Transformer:
    def __init__(self, admarker_files):
        self.admarker_files = admarker_files


    def transform(self):
        results = []
        for admarker_file in self.admarker_files:
            if os.path.splitext(admarker_file)[-1] in ['.xlsx', '.xls']:
                results.append(self.excel_transformation(admarker_file))
            else:
                results.append(self.xml_transformation(admarker_file))
        return results
    
    def excel_transformation(self, admarker_file):
        try:   
            wb = load_workbook(admarker_file)
            ws = wb.active
            for column in range(1, ws.max_column):
                if ws.cell(row=1, column=column).value == config.excel_admarker_field:
                    return ws.cell(row=2, column=column).value
            logger.info(f'There are no AdMarkers in: {admarker_file}. Ad Marker field in Jira will be set to null')
        except Exception as e:
            logger.error(f"Admarker not extracted from Excel file. Error {e}")
            return None        



    def xml_transformation(self, admarker_file):
        try:
            tree = etree.parse(admarker_file)
            # Delete xmlns additions in tag names
            for elem in tree.getiterator():
                elem.tag = etree.QName(elem).localname
            etree.cleanup_namespaces(tree)
            # Obtain values from admarker tags
            segments = tree.xpath('|'.join(config.xml_admarker_xpaths))
            if not segments:
                logger.info(f'There are no AdMarkers in: {admarker_file}. Ad Marker field in Jira will be set to null')
                return None
            tag_values = [segment.text for segment in segments]
            # Transform into VU standard
            if segments[0].tag == 'AdMarks':
                return ''.join(tag_values)
            elif segments[0].tag == 'tc_in':
                string_segments = '-'.join(tag_values)
                # Add wierd character
                wierd_char = re.sub('(-[^-]*)-', r'\1¬', string_segments)
                arr_segments = wierd_char.split('¬')
                # Back to string again
                return ''.join([f'[P{idx+1}]{item}¬' for idx, item in enumerate(arr_segments)])[:-1]
                
        except Exception as e:
            logger.error(f"Admarker not extracted from Xml file. Error {e}")
            return None       

