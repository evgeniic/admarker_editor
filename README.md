# Ad Marker
The application extracts Ad Markers from an Excel or Xml file, transforms them into VU format and updates Jira ticket with new Ad Markers

## Modules
- **config.yaml**  
Stores static information related to Jira, structure of processed files, and job interwals
- **admarker_config.py**  
Changes data from yaml file to Config object properties
- **job_runner.py**  
Entrypoint to the app. Manages general flow and sends results in log
- **jira_api.py**  
Manages interactions with Jira: get list of tickets to update, updates tickets with transformed Ad Markers and comment with the link to original file with Ad Markers
- **transformer.py**  
Processes the files with Ad Markers. Excel and Xml formats currently supported. Requeired file structure picked up from config.yaml
- **Log.py**
Manages logging the conversion process

## Workflow  
1. The app filtered Jira tickets based on jql query from config.yaml
2. File path from "Ad Marker" field extracted
3. If file is Excel AdMarkers are extracted only. If file is xml with AdMarkers as integers, then AdMarkers are extracted only. If file is xml with AdMarkers as time, then AdMarkers are converted into VU format: **[P1]10:00:00:00-10:12:10:15¬[P2]10:12:13:17-10:28:51:07¬[P3]10:28:54:09-10:38:26:05¬[P4]10:38:29:07-10:46:15:13**
4. Processed AdMarkers sent back to Jira, saved to "Ad Markers" field
5. Comment added to the ticket with the original path to the file with AdMarkers 
