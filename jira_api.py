import json
import os
import time

import requests

import Log
from admarker_config import Config

config = Config().config
newlogger = Log.get_logger()

def get_job():
    # Send request to Jira to get list of pending jobs
    headers = {'Content-Type': 'application/json'}
    payload = {
        "jql": f'{config.jql_query} and \"{config.jira_admarker_field[1]}\" is not Empty and \"{config.jira_admarker_field[1]}\" !~ "P1¬"'}
    response = requests.post(f"{config.jira_api}/search", data=json.dumps(
        payload), headers=headers, auth=(config.jira_user, config.jira_password))
    if response.status_code != 200:
        newlogger.error(
            f"Jira API responded with status code: {response.status_code}")
        return((None, response.text))
    else:
        newlogger.info(
            f"Jira API responded with status code: {response.status_code}")
        response_data = json.loads(response.text)
        items = {}
        for item in response_data['issues']:
            filepath = item['fields'][config.jira_admarker_field[0]]
            if os.path.splitext(filepath)[-1] in config.allowed_extensions:
                items[item['key']] = filepath
        return list(items.keys()), list(items.values())


def update_ticket(ticket, admarker, admarker_file):
    try:
        # Define payload
        headers = {"Accept": "application/json",
                    'Content-Type': 'application/json'}
        payload = {"fields": {config.jira_admarker_field[0]: admarker},
        "update":{"comment":[{"add": {"body": f"Automatic message:\n Ad Marker processed from location: {admarker_file}"}}]}
        }

        # Send request to Jira to update tickets with new admarkers
        response = requests.put(f'{config.jira_api}/issue/{ticket}', headers=headers,
                                data=json.dumps(payload), auth=(config.jira_user, config.jira_password))
    except Exception as e:
        newlogger.error(
            f"Unable to reach Jira for ticket update. Error {e}")

    if response.status_code != 204:
        newlogger.error(
            f"Jira API responded with status code: {response.status_code}")
        return(response.text)
    else:
        newlogger.info(
            f"Jira API responded with status code: {response.status_code}")
        return(admarker)
