import time
from datetime import datetime
import Log

import jira_api
from admarker_config import Config
from transformer import Transformer



config = Config().config
newlogger = Log.get_logger()



def run_program():
    while True:
        try:
            issues, admarker_files = jira_api.get_job()
            newlogger.info(f"{len(admarker_files)} files received for processing from issues: {issues}")
        except Exception as e:
            admarker_files = None
            newlogger.error(f"Unable to reach Jira to get tickets for processing. Error msg: {e}")
            
        
        if issues and admarker_files:
            admarkers = Transformer(admarker_files).transform()
            newlogger.info(f"{len(admarkers)} admarkers from issues {issues} are ready to be sent back to Jira")
            for ticket, admarker, admarker_file in zip(issues, admarkers, admarker_files):
                try:
                    response = jira_api.update_ticket(ticket, admarker, admarker_file)
                    newlogger.info(f"Jira ticket {ticket} updated with {response}")
                except Exception as e:
                    newlogger.error(f"Unable to reach Jira for ticket {ticket} update. Error msg: {e}")
                    time.sleep(config.pause_between_calls)

        else:
            newlogger.info(f"No files received for processing. Waiting for {config.pause_between_calls} seconds before next call")
        
        time.sleep(config.pause_between_calls)
        


if __name__ == "__main__":
    newlogger.info("AdMarker successfully started at {}".format(datetime.now()))
    run_program()
 