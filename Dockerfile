FROM python:3.7-slim-buster

COPY requirements.txt /tmp/
RUN apt-get update && apt-get install -y gettext-base \
    && pip install --upgrade pip && pip install -r /tmp/requirements.txt

RUN mkdir /admarker_editor
COPY ./ /admarker_editor/

WORKDIR /admarker_editor
CMD ["python", "job_runner.py"]
